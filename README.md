# Catch the RAJ9

The python script we used to track the RAJ9 High Altitude Baloon from OK1RAJ (https://ok1raj.cz/) during Maker Faire Prague Online 2020 (https://www.makerfaireprague.online/). It can send the gathered information to a specified telegram channel or show the possition in a live map.

NOTE: The thing is pretty hacky, we wrote it overnight. We just thought we would put it out there in case it could be useful to someone.

# Requirements

 - Software Defined Radio - [RTL-SDR](https://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/) or similar
 - SDR control program - [GQRX](https://gqrx.dk/) or similar
 - [FLDIGI](https://github.com/w1hkj/fldigi/) or even better [dl-fldigi](https://github.com/jamescoxon/dl-fldigi) for uploading to [habhub.org](http://habhub.org) at the same time
 - the script also relies on the awesome [pyfldigi](https://github.com/KM4YRI/pyFldigi), [crccheck](https://pypi.org/project/crccheck/) and optionally [python-telegram-bot](https://pypi.org/project/python-telegram-bot/) and [selenium](https://pypi.org/project/selenium/) python modules.

# Setup

 1. connect RTL-SDR
 2. start GQRX and configure the correct frequency and modulation
 3. start FLDIGI/DL-FLDIGI
 4. set the correct decoding parameters in FLDIGI/DL-FLDIGI settings, in case of RAJ9 set the following parameters:
 ```
 Modem/TTY/Tx:
        Carrier shift: Custom
        Custom shift: 540
        Baud rate: 100
        Bits per character: 7
        Stop bits: 2
        Filter Adjustment: 1.3
 ```
 5. configure the XML-RPC API in FLDIGI/DL-FLDIGI settings:
 ```
     On fl-digi latest:
        Miscs/TCP-IP sessions:
            Lock: false
    On dl-fldigi:
        Rig/XML-RPC:
            Use XML-RPC program: true
 ```
 6. It should work now! Start the script.

If you want to use the telegram feature, you will have to set a valid bot key in the program.
If you want to use the live map view feature, you will have to get a geckodriver executable.
