import time
import datetime
import pyfldigi
import curses
import re
from crccheck.crc import Crc16CcittFalse

"""
Setup requiered on fldigi:
    Modem/TTY/Tx:
        Carrier shift: Custom
        Custom shift: 540
        Baud rate: 100
        Bits per character: 7
        Stop bits: 2
        Filter Adjustment: 1.3
    On fl-digi latest:
        Miscs/TCP-IP sessions:
            Lock: false
    On dl-fldigi:
        Rig/XML-RPC:
            Use XML-RPC program: true
"""
# wanna live mapy.cz in browser?
livebrowser = False
# wanna send msgs to the telegram channel?
sendtelegram = True

# init everything
stdscr = curses.initscr()
c = pyfldigi.Client()

stdscr.clear()
stdscr.refresh()
curses.noecho()
curses.cbreak()

stdscr.addstr(0, 0, "RAJ9 Tracker 0.2 - patek.cz 2020")
stdscr.addstr(1, 0, "Setting up... please wait")
stdscr.refresh()
time.sleep(1)
c.modem.name = "RTTY"
packetformat = re.compile(r'\$\$\$RAJ9,([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*),([\d.-]*)\*([0123456789ABCDEF]{4})')
rowcontents = ""
prevdata = ""
timeout = False
lastTelegramSent = 0

stdscr.addstr(1, 0, "Running..."+" "*20)
stdscr.addstr(4, 0, "Last recieved packet:"+" "*20)
stdscr.addstr(5, 0, "Last recieved packet time:"+" "*20)
stdscr.addstr(6, 0, "Last recieved latitude:"+" "*20)
stdscr.addstr(7, 0, "Last recieved longitude:"+" "*20)
stdscr.addstr(8, 0, "Last recieved altitude:"+" "*20)
stdscr.addstr(9, 0, "Last recieved temp:"+" "*20)
stdscr.addstr(10, 0, "Last recieved bat:"+" "*20)
stdscr.addstr(13, 0, "GPS formatted:"+" "*20)
stdscr.addstr(14, 0, "Mapy.cz link:"+" "*20)
stdscr.addstr(16, 0, "Last CRC status:"+" "*20)
stdscr.addstr(19, 0, "Last decoding status:"+" "*20)
stdscr.refresh()

if livebrowser:
    from selenium import webdriver
    driver=webdriver.Firefox()
if sendtelegram:
    import telegram
    bot = telegram.Bot(token='xxxx') # talk to @botfather to get this


while True:
    data = c.text.get_rx_data().decode()
    if "$$$" in (prevdata+data):
        #print("Packet detected!")
        started = time.time()
        while not "\n" in rowcontents:
            rowcontents += c.text.get_rx_data().decode()
            #print("Got new piece")
            if time.time()-started > 9:
                timeout = True
                break
            time.sleep(1)
        unparsedcontent = prevdata+data+rowcontents
        data,prevdata,rowcontents = "","",""
        if timeout:
            stdscr.addstr(19, 0, "Last decoding status: TO")
            stdscr.refresh()
            timeout = False
            continue
        if packetformat.search(unparsedcontent):
            content = packetformat.findall(unparsedcontent)[0]
            if Crc16CcittFalse.calc(unparsedcontent.split("$$$")[1].split("*")[0].encode()) == int(content[10],16):
                stdscr.addstr(16, 0, "Last CRC status: OK")
                stdscr.addstr(4, 0, "Last recieved packet: "+unparsedcontent.rstrip()+" "*40)
                stdscr.addstr(5, 0, "Last recieved packet time: "+content[1]+" "*40)
                stdscr.addstr(6, 0, "Last recieved latitude: "+content[2]+" "*40)
                stdscr.addstr(7, 0, "Last recieved longtitude: "+content[3]+" "*40)
                stdscr.addstr(8, 0, "Last recieved altitude: "+content[4]+" "*40)
                stdscr.addstr(9, 0, "Last recieved temp: "+content[7]+" "*40)
                stdscr.addstr(10, 0, "Last recieved bat: "+content[8]+" "*40)
                stdscr.addstr(13, 0, "GPS formatted: "+content[2]+","+content[3]+" "*40)
                stdscr.addstr(14, 0, "Mapy.cz link: https://en.mapy.cz/zakladni?x="+content[3]+"&y="+content[2]+"&z=16&source=coor&id="+content[3]+"%2C"+content[2])
                if time.time()-lastTelegramSent>90 and sendtelegram:
                    bot.send_message(chat_id="@raj9info", text=str(datetime.datetime.now())+"\nGPS: "+content[2]+","+content[3]+"\nALT: "+content[4]+"\nTEMP: "+content[7]+"\nBAT: "+content[8]+"\nMapy.cz: https://en.mapy.cz/zakladni?x="+content[3]+"&y="+content[2]+"&z=16&source=coor&id="+content[3]+"%2C"+content[2])
                    lastTelegramSent = time.time()
                if livebrowser:
                    driver.get("https://en.mapy.cz/zakladni?x="+content[3]+"&y="+content[2]+"&z=16&source=coor&id="+content[3]+"%2C"+content[2])
            else:
                stdscr.addstr(16, 0, "Last CRC status: :(")

            stdscr.addstr(19, 0, "Last decoding status: OK")
        else:
            stdscr.addstr(19, 0, "Last decoding status: :(")
    stdscr.refresh()
    time.sleep(1)
    prevdata = data
